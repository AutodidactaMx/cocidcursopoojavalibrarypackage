/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operaciones;

public class Calculos {

    public static double sumar(Double numero1, Double numero2) {
        double total = numero1;
        total += numero2;
        return total;
    }

    public static double restar(Double numero1, Double numero2) {
        double total = numero1;
        total -= numero2;
        return total;
    }

    //Con numeros reales
    public static double divisionNReales(double dividiendo, double divisor) {
        double total = 0d;
        total = dividiendo / divisor;
        return total;
    }

    //Con numeros reales
    public static double division(int dividiendo, int divisor) {
        double total = 0d;
        total = dividiendo / divisor;
        return total;
    }

    public static void resultadoNegativo(double num) throws IllegalStateException {
        if (num < 0) {
            throw new IllegalStateException("El resultado no puede ser negativo");
        }
    }
}
